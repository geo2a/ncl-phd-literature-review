### Programming languages / Tools

#### [Haskell](https://www.haskell.org/)

##### Liquid Haskell

* [Towards Complete Specification and Verification with SMT](http://ani.sh/popl18.pdf)

#### [Idris](https://www.idris-lang.org/)

Idris is a general purpose pure functional programming language with dependent types. Dependent types allow types to be predicated on values, meaning that some aspects of a program’s behaviour can be specified precisely in the type. It is compiled, with eager evaluation.

#### [F\*](https://www.fstar-lang.org/)

F* (pronounced F star) is an ML-like functional programming language aimed at program verification. Its type system includes polymorphism, dependent types, monadic effects, refinement types, and a weakest precondition calculus. Together, these features allow expressing precise and compact specifications for programs, including functional correctness and security properties. The F* type-checker aims to prove that programs meet their specifications using a combination of SMT solving and manual proofs. Programs written in F* can be translated to OCaml, F#, or C for execution.

#### [Dafny](https://github.com/Microsoft/dafny)

Dafny is a programming language with a program verifier. As you type in your program, the verifier constantly looks over your shoulders and flags any errors.

### [TLA+](https://en.wikipedia.org/wiki/TLA%2B)

TLA+ is a formal specification language developed by Leslie Lamport. It is used to design, model,
document, and verify concurrent systems. TLA+ has been described as exhaustively-testable pseudocode
and blueprints for software systems; the TLA stands for "Temporal Logic of Actions."

### Projects

https://www.cl.cam.ac.uk/~pes20/rems/rems-all.html

#### [riscv-semantics](https://github.com/mit-plv/riscv-semantics)

riscv-semantics is a formal specification of the RISC-V ISA written in Haskell.

#### [Copilot](https://github.com/Copilot-Language)

A (Haskell DSL) stream language for generating hard real-time C code. Has a lot of SMT-solving/model-checking.

#### [HipSpec](https://github.com/danr/hipspec)

HipSpec is an inductive theorem prover for Haskell programs.

#### [Haskell Contracts Checker](https://github.com/danr/contracts)

Halo --- Haskell -> Logic. Check contracts on Haskell programs with an SMT solver.

Paper: https://www.microsoft.com/en-us/research/wp-content/uploads/2016/07/hcc-popl.pdf?from=https%3A%2F%2Fresearch.microsoft.com%2Fen-us%2Fum%2Fpeople%2Fsimonpj%2Fpapers%2Fverify%2Fhcc-popl.pdf

### Papers / Blogposts

#### [An ASM Monad](http://wall.org/~lewis/2013/10/15/asm-monad.html)

#### [Monads to Machine Code](http://www.stephendiehl.com/posts/monads_machine_code.html)

#### [Analyzing software requirements errors in safety-critical embedded systems](https://blog.acolyer.org/2017/12/01/analyzing-software-requirements-errors-in-safety-critical-embedded-systems/amp/).

#### [The role of software in spacecraft accidents](https://blog.acolyer.org/2017/11/30/the-role-of-software-in-spacecraft-accidents/).

#### [Submitting Haskell functions to Z3](http://newartisans.com/2017/04/haskell-and-z3/)

#### [Painless NP-complete problems: an embedded DSL for SMT solving](https://donsbot.wordpress.com/2011/01/16/painless-np-complete-problems-an-embedded-dsl-for-smt-solving/)

#### [Formal Verification: The Gap Between Perfect Code and Reality](https://raywang.tech/2017/12/20/Formal-Verification:-The-Gap-between-Perfect-Code-and-Reality/)

Some nicely-structured thoughts on what FM are capable of, and what they are not (really).

#### [Bidirectional ARM Assembly Syntax ] (https://alastairreid.github.io/bidirectional-assemblers/)

Alastair Reid's executable ARM specification as assembler/disassembler.

#### [An Empirical Study on the Correctness of Formally Verified Distributed Systems](https://homes.cs.washington.edu/~pfonseca/papers/eurosys2017-dsbugs.pdf)

#### [Coq: The world’s best macro assembler?](https://www.microsoft.com/en-us/research/publication/coq-worlds-best-macro-assembler/)

We describe a Coq formalization of a subset of the x86 architecture. 

#### [Formally Verified Software in the Real World](http://ssrg.nicta.com.au/publications/csiro_full_text/Klein_AKMHF_toappear.pdf)

Using seL4 -- a formally verified microkernel to develop high assurance systems (Isabelle/HOL). Good list of real-world usage of FV references in the section "2. FORMAL VERIFICATION"

#### [Awesome Cold Showers](https://github.com/hwayne/awesome-cold-showers)

A curated list of anti-hype papers (includes some on formal methods).

#### [A Trustworthy Monadic Formalization of the ARMv7 Instruction Set Architecture](https://www.cl.cam.ac.uk/~mom22/itp10-armv7.pdf)

Modelling ARMv7 in HOL4.

#### [How to prove a compiler correct](https://dbp.io/essays/2018-01-16-how-to-prove-a-compiler-correct.html)

#### [Randomised testing of a microprocessor model using SMT-solver state generation](http://homepages.inf.ed.ac.uk/bcampbe2/rems/scico2015.pdf)

Isabell/HOL4 model of a processor + SMT solver to find test cases.

#### [From System F to Typed Assembly Language](https://www.cs.princeton.edu/~dpw/papers/tal-toplas.pdf)

Plus an implementation by Stephanie Weirich https://github.com/sweirich/tal

#### [A monadic framework for relational verification: applied to information security, program equivalence, and optimizations](https://dl.acm.org/citation.cfm?id=3167090)

#### [Compilation as a Typed EDSL-to-EDSL Transformation](https://arxiv.org/html/1603.08865v3)

Descibe syntax and semantics of imperative programs using deep embedding of monadic computations (Kinda similar to Operational).

#### [An integrated concurrency and core-ISA architectural envelope definition, and test oracle, for IBM POWER multiprocessors](http://delivery.acm.org/10.1145/2840000/2830775/p635-gray.pdf?ip=128.240.225.74&id=2830775&acc=ACTIVE%20SERVICE&key=BF07A2EE685417C5%2E708BE730B8E35B42%2E4D4702B0C3E38B35%2E4D4702B0C3E38B35&__acm__=1532221174_7efad3e64094ee47520f9dc032d1c92f)

### PhD Thesises

#### [Verifying safety properties of Lustre programs: an SMT-based approach](http://clc.cs.uiowa.edu/Kind/Papers/Hag-PHD-08.pdf)
